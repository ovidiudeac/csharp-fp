﻿using System;
using System.IO;
using System.Diagnostics;
using System.Linq;
using System.Collections.Generic;
using datastructures;
using csfplinq;

namespace problem1
{
    class Program
    {
        static void Print(Tree t)
        {
            foreach (var v in t.InOrder())
            {
                Console.Write("{0} ", v);
            }
            Console.WriteLine();
        }

        static int SmalestFibonacciGreaterThanLimit(int limit)
        {
            throw new NotImplementedException();
        }

        static List<int> SmalestNFibonaccisGreaterThanLimit(int n, int limit)
        {
            throw new NotImplementedException();
        }

        static int SmallestItemGreaterThanLimit(Tree t, int limit)
        {
            throw new NotImplementedException();
        }

        static int NthItemInAscendingOrder(Tree t, int n)
        {
            throw new NotImplementedException();
        }

        static void Main(string[] args)
        {
            var t = Tree.From(1, 9, 4, 6, 3, 2, 8);

            Print(t);

            Console.WriteLine("SmallestGreaterThanLimit: {0}", 
                              SmalestFibonacciGreaterThanLimit(6)); // => 8

            SmalestNFibonaccisGreaterThanLimit(3, 10)
                .Print("SmalestNFibonaccisGreaterThanLimit"); // => 13, 21, 44

            Console.WriteLine("SmallestItemGreaterThanLimit: {0}",
                              SmallestItemGreaterThanLimit(t, 6)); // => 8
            
            Console.WriteLine("NthItemInAscendingOrder: {0}",
                              NthItemInAscendingOrder(t, 5)); // => 6
            

        }
    }
}
