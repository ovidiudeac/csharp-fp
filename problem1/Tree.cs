﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace datastructures
{
    public class Tree
    {
        private class Node
        {
            public Node(int v, Node l, Node r)
            {
                value = v;
                left = l;
                right = r;
            }
            public readonly int value;
            public readonly Node left;
            public readonly Node right;
        }

        private readonly Node root;

        public static Tree Empty()
        {
            return new Tree(null);
        }

        public static Tree From(params int[] values)
        {

            return new Tree(InsertAll(null, values));
        }

        private Tree(Node root)
        {
            this.root = root;
        }

        private static Node InsertAll(Node root, int[] values)
        {
            var r = root;
            foreach (var v in values)
            {
                r = Insert(r, v);
            }
            return r;
        }
        private static Node Insert(Node root, int value)
        {
            if (root == null) return new Node(value, null, null);

            if (value > root.value)
                return new Node(root.value, root.left, Insert(root.right, value));
            else
                return new Node(root.value, Insert(root.left, value), root.right);
        }

        public List<int> InOrder() => InOrder(root);

        private static List<int> InOrder(Node r)
        {
            if (r == null) return new List<int>();

            var result = InOrder(r.left);

            result.Add(r.value);

            result.AddRange(InOrder(r.right));
            return result;
        }
    }
}
