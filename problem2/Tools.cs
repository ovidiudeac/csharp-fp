﻿using System;
using System.Collections.Generic;
using System.Text;

namespace problem2
{
    public static class Tools
    {

        public static string ToString<T>(this IEnumerable<T> list, string separator)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var obj in list)
            {
                if (sb.Length > 0)
                {
                    sb.Append(separator);
                }
                sb.Append(obj);
            }
            return sb.ToString();
        }

    }
}
