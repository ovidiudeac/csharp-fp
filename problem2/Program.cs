﻿using System;
using System.Reactive;
using System.Reactive.Linq;
using System.Linq;
using Microsoft.Reactive.Testing;
using myrx;
using System.Reactive.Concurrency;
using System.Collections.Generic;

namespace problem2
{
    class MainClass
    {
        static int count = 3;
        static TimeSpan minTimeSpan = TimeSpan.FromSeconds(5);

        private static TimeSpan SumBufferDuration<T>(IEnumerable<Timestamped<T>> buffer)
        {
            return buffer.Last().Timestamp - buffer.First().Timestamp;
        }

        static IObservable<T> DosProtect<T>(IObservable<T> input, IScheduler scheduler)
        {
            var conditional =
                input.Timestamp(scheduler)
                     .Buffer(count, 1)
                     //.Do(x => Console.WriteLine("buffer={0}", x.Select(a => a.Value).ToString(",")))
                     .Select(buffer => new { value = buffer.Last().Value, timeSpan = SumBufferDuration(buffer)})
                     //.Do(x => Console.WriteLine("x={0}", x))
                     .TakeWhile(buffer => buffer.timeSpan > minTimeSpan)
                     .Select(buffer => buffer.value);

            var unconditional = input.Take(count - 1);

            return unconditional.Concat(conditional);
        }

        public static void Main(string[] args)
        {
            var scheduler = ThreadPoolScheduler.Instance;
            //var scheduler = new TestScheduler();

            //                            0  1   2   3   4   5   6   7   8   9
            var input = MyObservable.Just(0, 2, 12, 23, 24, 25, 26, 38, 42, 54)
                                    .Select((t, i) => new { timespan = TimeSpan.FromSeconds(t), 
                                                            value = String.Format("Event-{0}",i) })
                                    .Select(x => Observable.Timer(x.timespan, scheduler)
                                                           .Select(t => x.value))
                                    .Merge();
            

            var output = DosProtect(input, scheduler);

            input.Timestamp(scheduler).Print("Input");
            //output.Print("Output");

            //scheduler.AdvanceTo(TimeSpan.TicksPerSecond * 60);
            Console.ReadLine();
        }
    }
}
