﻿using System;
using System.Collections.Generic;
using System.Reactive.Disposables;

namespace myrx
{
    static class MyObservable
    {
        public static IDisposable Print<TIn>(this IObservable<TIn> input, String msg)
        {
            return input.Subscribe(x => Console.WriteLine("{0}: {1}", msg, x));
        }

        public static IObservable<T> Just<T>(params T[] vs)
        {
            return new FromImpl<T>(vs);
        }

        internal class FromImpl<T> : IObservable<T>
        {
            private IEnumerable<T> items;

            public FromImpl(IEnumerable<T> items)
            {
                this.items = items;
            }

            public IDisposable Subscribe(IObserver<T> observer)
            {
                foreach (var x in items)
                    observer.OnNext(x);

                observer.OnCompleted();
                return Disposable.Empty;
            }
        }
    }
}
