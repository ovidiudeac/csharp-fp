﻿using System;

namespace myrx.Linq
{
    internal class JoinObservable<TOut> : IObservable<TOut>
    {
        private IObservable<IObservable<TOut>> observable;

        public JoinObservable(IObservable<IObservable<TOut>> observable)
        {
            this.observable = observable;
        }

        public IDisposable Subscribe(IObserver<TOut> observer)
        {
            return observable.Subscribe(inner => inner.Subscribe(observer));
        }
    }
}