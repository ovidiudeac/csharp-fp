﻿using System;

namespace myrx.Linq
{
    internal class TakeObserver<T> : IObserver<T>
    {
        private int n;
        private readonly IObserver<T> obs;

        public TakeObserver(IObserver<T> obs, int n)
        {
            this.obs = obs;
            this.n = n;
        }

        public void OnCompleted()
        {
            obs.OnCompleted();
        }

        public void OnError(Exception error)
        {
            obs.OnError(error);
        }

        public void OnNext(T value)
        {
            if (n > 0)
            {
                obs.OnNext(value);
                n = n - 1;
            }
            else obs.OnCompleted();
        }
    }
}