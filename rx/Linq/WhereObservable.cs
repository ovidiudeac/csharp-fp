﻿using System;

namespace myrx.Linq
{
    internal class WhereObservable<T> : IObservable<T>
    {
        private IObservable<T> input;
        private Func<T, bool> p;

        public WhereObservable(IObservable<T> input, Func<T, bool> p)
        {
            this.input = input;
            this.p = p;
        }

        public IDisposable Subscribe(IObserver<T> observer)
        {
            return input.Subscribe(new WhereObserver(observer, p));
        }

        private class WhereObserver : IObserver<T>
        {
            private IObserver<T> observer;
            private Func<T, bool> p;

            public WhereObserver(IObserver<T> observer, Func<T, bool> p)
            {
                this.observer = observer;
                this.p = p;
            }

            public void OnCompleted()
            {
                observer.OnCompleted();
            }

            public void OnError(Exception error)
            {
                observer.OnError(error);
            }

            public void OnNext(T value)
            {
                if (p(value))
                    observer.OnNext(value);
            }
        }
    }
}
