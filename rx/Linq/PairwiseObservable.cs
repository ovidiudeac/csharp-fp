﻿using System;

namespace myrx.Linq
{
    internal class PairwiseObservable<TIn, TOut> : IObservable<TOut>
    {
        private IObservable<TIn> input;
        private Func<TIn, TIn, TOut> f;

        public PairwiseObservable(IObservable<TIn> input, Func<TIn, TIn, TOut> f)
        {
            this.input = input;
            this.f = f;
        }

        public IDisposable Subscribe(IObserver<TOut> observer)
        {
            return input.Subscribe(new PairwiseObserver(observer, f));
        }

        private class PairwiseObserver : IObserver<TIn>
        {
            private IObserver<TOut> observer;
            private readonly Func<TIn, TIn, TOut> f;

            public PairwiseObserver(IObserver<TOut> observer, Func<TIn, TIn, TOut> f)
            {
                this.observer = observer;
                this.f = f;
            }

            public void OnCompleted()
            {
                throw new NotImplementedException();
            }

            public void OnError(Exception error)
            {
                throw new NotImplementedException();
            }

            public void OnNext(TIn value)
            {
                throw new NotImplementedException();
            }
        }
    }
}