﻿using System;
using System.Collections.Generic;
using System.Reactive.Disposables;

namespace myrx.Linq
{
    internal class FromImpl<T> : IObservable<T>
    {
        private IEnumerable<T> items;

        public FromImpl(IEnumerable<T> items)
        {
            this.items = items;
        }

        public IDisposable Subscribe(IObserver<T> observer)
        {
            foreach (var x in items)
                observer.OnNext(x);

            observer.OnCompleted();
            return Disposable.Empty;
        }
    }
}
