﻿using System;
using System.Collections.Generic;

namespace myrx.Linq
{
    public static class MyRx
    {
        public static IObservable<T> From<T>(IEnumerable<T> items) 
        {
            return new FromImpl<T>(items);
        }

        public static IObservable<T> Just<T>(params T[] items)
        {
            return new FromImpl<T>(items);
        }

        public static IDisposable Print<TIn>(this IObservable<TIn> input, String msg)
        {
            return input.Subscribe(x => Console.WriteLine("{0}: {1}", msg, x));
        }

        public static IObservable<TOut> Select<TIn,TOut>(this IObservable<TIn> input, Func<TIn, TOut> f)
        {
            return new SelectObservable<TIn, TOut>(input, f);
        }

        public static IObservable<TOut> SelectMany<TIn, TOut>(this IObservable<TIn> input, Func<TIn, IObservable<TOut>> f)
        {
            IObservable<IObservable<TOut>> tmp = input.Select(f);
            return new JoinObservable<TOut>(tmp);
        }

        public static IObservable<T> Where<T>(this IObservable<T> input, Func<T, bool> p)
        {
            return new WhereObservable<T>(input, p);
        }

        public static IObservable<TOut> Pairwise<TIn, TOut>(this IObservable<TIn> input, Func<TIn, TIn, TOut> f)
        {
            return new PairwiseObservable<TIn, TOut>(input, f);
        }

        public static IObservable<T> Create<T>(Func<IObserver<T>, Action> subscribe) 
        {
            return new CreateObservable<T>(subscribe);
        }


        public static IObservable<T> Take<T>(this IObservable<T> input, int n)
        {
            return Create((IObserver<T> obs) =>
            {
                var subscription = input.Subscribe(new TakeObserver<T>(obs, n));
                return () => subscription.Dispose();
            });
        }

        public static IObservable<T> Skip<T>(this IObservable<T> input, int n)
        {
            return Create((IObserver<T> obs) =>
                    {
                        var remaining = n;
                        var subscription = input.Subscribe(v =>
                                                                {
                                                                    if (remaining == 0) obs.OnNext(v);
                                                                    else remaining = remaining - 1;
                                                                },
                                                           obs.OnError,
                                                           obs.OnCompleted);

                        return subscription.Dispose;
                    });
        }

    }
}
