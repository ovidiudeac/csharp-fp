﻿using System;

namespace myrx.Linq
{
    internal class SelectObservable<TIn, TOut> : IObservable<TOut>
    {
        private readonly IObservable<TIn> input;
        private readonly Func<TIn, TOut> f;

        public SelectObservable(IObservable<TIn> input, Func<TIn, TOut> f)
        {
            this.input = input;
            this.f = f;
        }

        public IDisposable Subscribe(IObserver<TOut> observer)
        {
            return input.Subscribe(new SelectObserver(observer, f));
        }

        private class SelectObserver : IObserver<TIn>
        {
            private readonly IObserver<TOut> observer;
            private readonly Func<TIn, TOut> f;

            public SelectObserver(IObserver<TOut> observer, Func<TIn, TOut> f)
            {
                this.observer = observer;
                this.f = f;
            }

            public void OnCompleted()
            {
                observer.OnCompleted();
            }

            public void OnError(Exception error)
            {
                observer.OnError(error);
            }

            public void OnNext(TIn value)
            {
                observer.OnNext(f(value));
            }
        }
    }
}
