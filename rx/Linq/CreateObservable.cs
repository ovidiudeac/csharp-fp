﻿using System;

namespace myrx.Linq
{
    internal class CreateObservable<T> : IObservable<T>
    {
        private Func<IObserver<T>, Action> subscribe;

        public CreateObservable(Func<IObserver<T>, Action> subscribe)
        {
            this.subscribe = subscribe;
        }

        public IDisposable Subscribe(IObserver<T> observer)
        {
            Action whenDisposed = subscribe(observer);
            return new MyDisposable(whenDisposed);
        }

        private class MyDisposable : IDisposable
        {
            private Action whenDisposed;

            public MyDisposable(Action whenDisposed)
            {
                this.whenDisposed = whenDisposed;
            }

            public void Dispose()
            {
                whenDisposed.Invoke();
            }
        }
    }
}