﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using myrx.Linq;

namespace myrx
{
    class Program
    {
        static void Main(string[] args)
        {
            var obs = MyRx.Just(4, 3, 2, 1, 2, 3, 4);

            obs.Print("Initial sequence");

            obs.Select(x => x + 20).Print("After select");

            obs.Where(x => x > 2).Print("After Where");

            //obs.Pairwise((x,y) => x + y).Print("After pairwise");

            obs.Take(3).Print("After Take");
        }
    }
}
