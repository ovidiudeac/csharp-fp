﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Security.Cryptography.X509Certificates;
//using csfplinq.tools.MyLinq;
using csfplinq.tools.MyLinqYield;

namespace csfplinq
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var lst = new List<int> { 1, 2, -1, 11, -2, 7 };

            lst.Print("Original sequence");

            lst.Select(x => x + 1)
                .Select(x => x.ToString())
                .Print("After Select");

            //lst.Where(x => x > 1)
                //.Print("After Where");

            lst.SelectMany(x => System.Linq.Enumerable.Repeat(x, 3))
                .Print("After SelectMany");

            //lst.Pairwise((x, y) => x + y)
            //.Print("Example Pairwiseß"); // => 3,1,10,9,5

            var output = lst.Select(x => { if (x > 0) return x; else throw new Exception("negative"); })
                            .Catch((Exception x) => new List<int>() { 100, 200, 300 });

            foreach (var x in output)
                Console.WriteLine(x);
        }
    }
}
