using System;
using System.Collections.Generic;

namespace csfplinq
{
    public static class HelperExtensions
    {
        public static void Print<T>(this IEnumerable<T> result, String msg)
        {
            Console.Write("{0}: [ ", msg);
            foreach (var x in result)
            {
                Console.Write("{0} ", x);
            }
            Console.WriteLine("]");
        }
    }
}