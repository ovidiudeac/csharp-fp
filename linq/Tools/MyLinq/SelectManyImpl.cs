﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace csfplinq.Tools.MyLinq
{
    public class SelectManyImpl<U,V> : IEnumerable<V>
    {
        private readonly Func<U, IEnumerable<V>> _f;
        private readonly IEnumerable<U> _input;

        public SelectManyImpl(IEnumerable<U> input, Func<U, IEnumerable<V>> f)
        {
            this._input = input;
            this._f = f;
        }

        public IEnumerator<V> GetEnumerator()
        {
            IEnumerator<IEnumerator<V>> innerEnumerator = 
                new SelectEnumerator<U, IEnumerator<V>>(
                    _input.GetEnumerator(),
                    u => _f(u).GetEnumerator());

            return new ConcatEnumerator<V>(innerEnumerator);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }






    internal class EmptyEnumerator<T> : IEnumerator<T>
    {
        public void Dispose() {}

        public bool MoveNext()
        {
            return false;
        }

        public void Reset() {}

        public T Current
        {
            get { throw new IndexOutOfRangeException(); }
        }

        object IEnumerator.Current { get => Current; }
    }



    internal class ConcatEnumerator<U> : IEnumerator<U>
    {
        private readonly IEnumerator<IEnumerator<U>> _inputEnum;
        private IEnumerator<U> _innerEnum = new EmptyEnumerator<U>();

        public ConcatEnumerator(IEnumerator<IEnumerator<U>> inputEnum)
        {
            this._inputEnum = inputEnum;
        }

        public void Dispose()
        {
            _inputEnum.Dispose();
            _innerEnum.Dispose();
        }

        public bool MoveNext()
        {
            if (_innerEnum.MoveNext())
            {
                return true;
            }
            else
            {
                if (_inputEnum.MoveNext())
                {
                    SetInner(_inputEnum.Current);
                    Debug.Assert(_innerEnum != null, "_innerEnum != null");

                    return _innerEnum.MoveNext();
                }
                else
                {
                    return false;
                }
            }
        }

        public void Reset()
        {
            _inputEnum.Reset();
            SetInner(new EmptyEnumerator<U>());
        }

        public U Current {
            get
            {
                return _innerEnum.Current;
            }
        }
        object IEnumerator.Current {
            get
            {
                return Current;
            }
        }

        private void SetInner(IEnumerator<U> newValue)
        {
            _innerEnum.Dispose();
            _innerEnum = newValue;
        }
    }
}