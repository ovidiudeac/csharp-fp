﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace csfplinq.tools.MyLinq
{
    internal class PairwiseImpl<TIn, TOut> : IEnumerable<TOut>
    {
        private IEnumerable<TIn> input;
        private Func<TIn, TIn, TOut> f;

        public PairwiseImpl(IEnumerable<TIn> input, Func<TIn, TIn, TOut> f)
        {
            this.input = input;
            this.f = f;
        }

        public IEnumerator<TOut> GetEnumerator()
        {
            return new PairwiseEnum<TOut, TIn>(this.input.GetEnumerator(), f);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    internal class PairwiseEnum<TOut, TIn> : IEnumerator<TOut>
    {
        private IEnumerator<TIn> enumerator;
        private Func<TIn, TIn, TOut> f;

        public PairwiseEnum(IEnumerator<TIn> enumerator, Func<TIn, TIn, TOut> f)
        {
            this.enumerator = enumerator;
            this.f = f;
        }

        object IEnumerator.Current {
            get
            {
                return this.Current;
            }
        }

        public TOut Current {
            get
            {
                throw new NotImplementedException();
            }
        }

        public bool MoveNext()
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public void Reset()
        {
            throw new NotImplementedException();
        }
    }
}