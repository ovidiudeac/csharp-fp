﻿using csfplinq.Tools.MyLinq;
using System;
using System.Collections.Generic;

namespace csfplinq.tools.MyLinq
{
    public static class MyLinq
    {
        public static IEnumerable<TOut> Select<TIn, TOut>(this IEnumerable<TIn> input, Func<TIn, TOut> f)
        {
            return new SelectImpl<TIn, TOut>(input, f);
        }

        public static IEnumerable<TOut> SelectMany<TIn, TOut>(this IEnumerable<TIn> input, Func<TIn, IEnumerable<TOut>> f)
        {
            return new SelectManyImpl<TIn, TOut>(input, f);
        }

        public static IEnumerable<TValue> Where<TValue>(this IEnumerable<TValue> input, Func<TValue, bool> p)
        {
            return new WhereImpl<TValue>(input, p);
        }

        public static IEnumerable<TOut> Pairwise<TIn, TOut>(this IEnumerable<TIn> input, Func<TIn, TIn, TOut> f)
        {
            return new PairwiseImpl<TIn, TOut>(input, f);
        }
    }
}