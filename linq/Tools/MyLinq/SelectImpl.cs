﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace csfplinq.Tools.MyLinq
{
    public class SelectImpl<U,V> : IEnumerable<V>
    {
        private readonly Func<U, V> _f;
        private readonly IEnumerable<U> _input;

        public SelectImpl(IEnumerable<U> input, Func<U, V> f)
        {
            this._input = input;
            this._f = f;
        }

        public IEnumerator<V> GetEnumerator()
        {
            return new SelectEnumerator<U, V>(_input.GetEnumerator(), _f);

        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }








    internal class SelectEnumerator<U, V> : IEnumerator<V>
    {
        private readonly Func<U, V> _f;
        private readonly IEnumerator<U> _inputEnum;

        public SelectEnumerator(IEnumerator<U> inputEnum, Func<U, V> f)
        {
            this._f = f;
            this._inputEnum = inputEnum;
        }

        public void Dispose()
        {
            _inputEnum.Dispose();
        }

        public bool MoveNext()
        {
            return _inputEnum.MoveNext();
        }

        public void Reset()
        {
            _inputEnum.Reset();
        }

        public V Current {
            get
            {
                return _f(_inputEnum.Current);
            }
        }

        object IEnumerator.Current { get => Current; }
    }
}