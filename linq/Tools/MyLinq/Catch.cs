﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace csfplinq.Tools.MyLinq
{
    internal class CatchImpl<TValue, TExc> : IEnumerable<TValue>
        where TExc : Exception
    {
        private readonly IEnumerable<TValue> _input;
        private readonly Func<TExc, TValue> _f;

        public CatchImpl(IEnumerable<TValue> input, Func<TExc, TValue> f)
        {
            this._input = input;
            this._f = f;
        }

        internal class CatchEnumerator : IEnumerator<TValue>
        {
            private readonly Func<TExc, TValue> _f;
            private readonly IEnumerator<TValue> _inputEnumerator;
            private TExc _ex;

            public CatchEnumerator(IEnumerable<TValue> input, Func<TExc, TValue> f)
            {
                this._inputEnumerator = input.GetEnumerator();
                this._f = f;
            }

            public void Dispose()
            {
                _inputEnumerator.Dispose();
            }

            public bool MoveNext()
            {
                _ex = null;

                try
                {
                    return _inputEnumerator.MoveNext();
                }
                catch (TExc ex)
                {
                    _ex = ex;
                    return true;
                }
            }

            public void Reset()
            {
                _inputEnumerator.Reset();
            }

            public TValue Current
            {
                get
                {
                    if (_ex == null)
                        return _inputEnumerator.Current;
                    else
                    {
                        return _f(_ex);
                    }
                }
            }

            object IEnumerator.Current => Current;
        }

        public IEnumerator<TValue> GetEnumerator()
        {
            return new CatchEnumerator(_input, _f);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}