﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace csfplinq.tools.MyLinq
{
    internal class WhereImpl<TValue> : IEnumerable<TValue>
    {
        private IEnumerable<TValue> input;
        private Func<TValue, bool> p;

        public WhereImpl(IEnumerable<TValue> input, Func<TValue, bool> p)
        {
            this.input = input;
            this.p = p;
        }

        public IEnumerator<TValue> GetEnumerator()
        {
            return new WhereEnumerator<TValue>(input.GetEnumerator(), p);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    internal class WhereEnumerator<TValue> : IEnumerator<TValue>
    {
        private IEnumerator<TValue> enumerator;
        private Func<TValue, bool> p;

        public WhereEnumerator(IEnumerator<TValue> enumerator, Func<TValue, bool> p)
        {
            this.enumerator = enumerator;
            this.p = p;
        }

        public TValue Current { get => enumerator.Current; }

        object IEnumerator.Current { get => Current; }

        public void Dispose()
        {
            enumerator.Dispose();
        }

        public bool MoveNext()
        {
            while (true)
            {
                if (enumerator.MoveNext())
                {
                    if (p(enumerator.Current))
                        return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public void Reset()
        {
            enumerator.Reset();
        }
    }
}