using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;

namespace csfplinq.tools.MyLinqYield
{
    static class MyLinqYield
    {
        public static IEnumerable<TOut> Select<TIn, TOut>(this IEnumerable<TIn> input, Func<TIn, TOut> f)
        {
            foreach (var x in input)
            {
                yield return f(x);
            }
        }


        public static IEnumerable<TOut> SelectMany<TIn, TOut>(this IEnumerable<TIn> input, Func<TIn, IEnumerable<TOut>> f)
        {
            foreach (var x in input)
            {
                foreach (var y in f(x))
                {
                    yield return y;
                }
            }
        }

        public static IEnumerable<TValue> Catch<TValue, TExc>(this IEnumerable<TValue> input, Func<TExc, IEnumerable<TValue>> f)
            where TExc : Exception
        {
            IEnumerable<TValue> alternative = new List<TValue>();

            using (var enumerator = input.GetEnumerator())
            {
                var hasNext = true;

                while (hasNext)
                {
                    var v = default(TValue);

                    try
                    {
                        hasNext = enumerator.MoveNext();
                        v = enumerator.Current;
                    }
                    catch (TExc ex)
                    {
                        alternative = f(ex);
                        break;
                    }

                    yield return v;
                }
            }
            foreach (var x in alternative)
                yield return x;
        }

    }
}